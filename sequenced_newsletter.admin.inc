<?php 
// Id

/**
 * @file
 * Administration pages for sequenced newsletter.
 */

/**
 * Administration pages form callback.
 */
function sequenced_newsletter_admin($form, &$form_state) {
  $form = array();
  $header = array(
    'name' => t('Sequenced newsletter name'),
    'status' => t('Status'),
    'operations' => t('Operations'),
  );
  $rows = _sequenced_newsletter_get_active_items();
  $form['table'] = array(
    '#markup' => theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No sequences.'))),
  );

  return $form;
}


/**
 * Form callback for sequenced newsletter edit form.
 */
function sequenced_newsletter_admin_edit($form, &$form_state, $id = -1) {
  
  // Load old data if editing existing entry.
  $data = array();
  if ($id > -1) {
    $data = _sequenced_newsletter_get_item($id);
  }
  $data += array(
    'name' => '',
    'enabled' => 0,
    'tid' => 0,
    'sort' => 0,
    'start_time' => date('Y-m-d H:i'),
    'reset' => 0,
    'interval' => 0,
  );
  
  // Build form.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Sequenced\'s newsletter name'),
    '#default_value' => $data['name'],
    '#description' => t('Enter human name for this newsletter. Name\'s length is limited to 128 characters.'),
    '#required' => TRUE,
  );

  $options = array(
    t('Disabled'),
    t('Enabled'),
  );

  $form['enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Enable this sequenced newsletter'),
    '#default_value' => $data['enabled'],
    '#description' => t('When enabled sequenced newsletters will be sent. When disabled nothing will happen.'),
    '#options' => $options,
  );

  $form['newsletter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Newsletter'),
  );

  $form['newsletter']['tid'] = array(
    '#type' => 'select',
    '#title' => t('Newsletter'),
    '#default_value' => $data['tid'],
    '#options' => _sequenced_newsletter_get_tids(),
    '#description' => t('Sequenced newsletter. Subscribers will get sequenced emails from this newsletter term.'),
    '#required' => TRUE,
  );

  $form['newsletter']['sort'] = array(
    '#type' => 'select',
    '#title' => t('Sorting field'),
    '#default_value' => $data['sort'],
    '#options' => _sequenced_newsletter_get_sort(),
    '#description' => t('Which field to use for ordering newsletter issues.'),
    '#required' => TRUE,
  );
 
  $form['timing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sequence timing'),
  );

  $form['timing']['start_time'] = array(
    '#type' => 'date_select',
    '#title' => t('Sequence start'),
    '#date_type' =>  DATE_DATETIME,
    '#default_value'=> $data['start_time'],
    '#date_format' => 'm-d-Y - H:i',
    '#date_timezone' => date_default_timezone(),
    '#description' => t('Time and day when emails will be moved into spool/sending will start.'),
    '#required' => TRUE
  );

  if ($id > -1) {
    $form['timing']['reset'] = array(
      '#type' => 'checkbox',
      '#title' => t('Reset time'),
      '#default_value' => $data['reset'],
      '#description' => t("Check if you want to reset timing of this newsletter. If you changed Sequence start time, then this is probbably a good idea.<br />WARNING: Mail will be sent immediatley if timer is reset and send time is set to the past."),
    );
  }
 
  $form['timing']['interval'] = array(
    '#type' => 'select',
    '#title' => t('Sending interval'),
    '#default_value' => $data['interval'],
    '#options' => _sequenced_newsletter_get_intervals(),
    '#description' => t("Interval to send sequenced newspetters."),
    '#required' => TRUE,
  );
 
  $form['sqnid'] = array(
    '#type' => 'hidden',
    '#value' => $id,
  );
 
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  if ($id > -1) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
  }

  return $form;
}

function sequenced_newsletter_admin_edit_validate($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] != t('Delete')) {
  }
}

function sequenced_newsletter_admin_edit_submit($form, &$form_state) {
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  if ($op == t('Delete')) {
    $form_state['redirect'] = 'admin/config/services/simplenews/sequenced_newsletter/' . $form_state['values']['sqnid'] . '/delete';
    return;
  }

  $date_obj = new DateTime($form_state['values']['start_time'], new DateTimeZone(date_default_timezone()));
  $date = $date_obj->getTimestamp();

  // Prepare data for DB.
  $insert = array(
    'name' => $form_state['values']['name'],
    'tid' => $form_state['values']['tid'],
    'start_date' => $date,
    'send_interval' => $form_state['values']['interval'],
    'status' => $form_state['values']['enabled'],
    'sort' => $form_state['values']['sort'],
  );

  // Write to DB.
  $db_response = '';
  if ($form_state['values']['sqnid'] == -1) {
    $db_response = drupal_write_record('sequenced_newsletter_conf', $insert);
  }
  else {
    // Reset last send if desired to do so.
    if ($form_state['values']['reset']) {
      $insert['last_send'] = 0;
      drupal_set_message(t("Sequenced newsletter <em>@name</em> was successfully reseted.", array('@name' => $form_state['values']['name'])));
    }
    $insert['sqnid'] = $form_state['values']['sqnid'];
    $db_response = drupal_write_record('sequenced_newsletter_conf', $insert, array( 'sqnid'));
  }

  // Verbose message.
  if ($db_response) {
    drupal_set_message(
      t("Sequenced newsletter <em>@name</em> was successfully saved.", array('@name' => $form_state['values']['name']))
    );
  }
  else {
    drupal_set_message(
      t("An error occured while trying to save <em>@name</em>.", array('@name' => $form_state['values']['name'])),
     	'error'
    );
  }
 
  // Redirect back to basic configuration.
  $form_state['redirect'] = 'admin/config/services/simplenews/sequenced_newsletter';
}

/**
 * Delete callback.
 */
function sequenced_newsletter_admin_delete($form, &$form_state, $id) {
  $result = db_select('sequenced_newsletter_conf', 'snc')
    ->fields('snc', array('name'))
    ->condition('sqnid', $id)
    ->execute()
    ->fetchAssoc();

  $form['sqnid'] = array(
    '#type' => 'value',
    '#value' => $id,
  );

  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $result['name'])),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/config/services/simplenews/sequenced_newsletter',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

function sequenced_newsletter_admin_delete_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    #db_query("UPDATE {sequenced_newsletter_conf} SET status=2 WHERE sqnid=%d", $form_state['values']['sqnid']);
    db_update('sequenced_newsletter_conf')
      ->fields(array('status' => 2))
      ->condition('sqnid', $form_state['values']['sqnid'])
      ->execute();
  }

  $form_state['redirect'] = 'admin/config/services/simplenews/sequenced_newsletter';
}


/**
 * Returns data about terms in Simplenews vocabulary.
 * 
 * @return array that can be used as options for select form element.
 */
function _sequenced_newsletter_get_tids() {
  //get simplenews terms
  $vid = variable_get('simplenews_vid', -1);
  $terms = db_select('taxonomy_term_data', 'td')
    ->fields('td', array('tid', 'name'))
    ->condition('vid', $vid)
    ->execute();
  $terms_array = array();
  while ($row = $terms->fetchAssoc()) {
    $terms_array[$row['tid']] = t($row['name']);
  }
  
  return $terms_array;
}

/**
 * Returns info about possible interval settings.
 * 
 * @return Array of possible intervals.
 */
function _sequenced_newsletter_get_intervals() {
  return array(
    0 => t('Daily'),
    1 => t('Weekly'),
    2 => t('Monthly'),
    3 => t('5 minutes'),
    4 => t('1 minute'),
  );
}

/**
 * Returns info about possible ordering settings.
 * 
 * @return Array of possible order fields.
 */
function _sequenced_newsletter_get_sort() {
  return array(
    0 => t('Node id (nid)'),
    1 => t('Node update time (changed)'),
    2 => t('Node create time (created)'),
  );
}

/**
 * Loads one newsletter from DB and returns it as array.
 * 
 * @param $id ID of seq. newsletter to be loaded from DB.
 * 
 * @return seq. newsletter as array.
 */
function _sequenced_newsletter_get_item($id) {
  $result = db_select('sequenced_newsletter_conf', 'snc')
    ->fields('snc', array('sqnid', 'name', 'status', 'tid', 'start_date', 'send_interval', 'sort'))
    ->condition('sqnid', $id)
    ->condition('status', 2, '<>')
    ->execute();
  if ($result) {
    $item = $result->fetchAssoc();
    return array(
      'name' => check_plain($item['name']),
      'enabled' => $item['status'],
      'tid' => $item['tid'],
      'start_time' => date('Y-m-d H:i', $item['start_date']),
      'interval' => $item['send_interval'],
      'sqnid' => $item['sqnid'],
      'sort' => $item['sort'],
    );
  }
  else {
    drupal_goto('admin/config/services/simplenews/sequenced_newsletter');
  }
}

/**
 * Returns all acitve items as an array.
 * 
 * @return Array that can be passed to theme_table().
 */
function _sequenced_newsletter_get_active_items() {
  $data = array();
  $result = db_select('sequenced_newsletter_conf', 'snc')
    ->fields('snc', array('sqnid', 'name', 'status'))
    ->condition('status', 2, '<>')
    ->orderBy('sqnid', 'ASC')
    ->execute();
  
  foreach ($result as $item) {
    $edit = l(t('Edit'), 'admin/config/services/simplenews/sequenced_newsletter/' . $item->sqnid . '/edit');
    $delete = l(t('Delete'), 'admin/config/services/simplenews/sequenced_newsletter/' . $item->sqnid . '/delete');
    
    $data[] = array(
      check_plain($item->name),
      $item->status ? t('Enabled') : t('Disabled'),
      $edit . ' ' . $delete, 
    );
  }
  
  return $data;
}
